var webpack = require("webpack"),
    webpackConfig = require('./webpack.config'),
    path = require("path");
    webpackConfig.devtool = 'inline-source-map';
    webpackConfig.entry =  {};


module.exports = function(config) {
    config.set({
        basePath: "",
        frameworks: ["jasmine"],
        files: [
            './app/*.js',
            "./spec/**/*.spec.js"
        ],
        preprocessors: {
            "./spec/**/*.spec.js": ["webpack"]
        },
        webpack: webpackConfig,

        webpackMiddleware: {
            noInfo: true
        },
        plugins: [
            require("karma-webpack"),
            require("karma-jasmine"),
            require("karma-chrome-launcher")
        ],
        reporters: ["dots"],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ["Chrome"],
        singleRun: false
    });
};