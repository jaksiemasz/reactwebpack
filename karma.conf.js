var webpack = require("webpack"),
    webpackConfig = require('./webpack.config'),
    path = require("path");
    webpackConfig.devtool = 'inline-source-map';
    //webpackConfig.entry =  {};

module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine"],
    files: [
      './app/*.jsx',
      "./spec/tests/*.spec.jsx"
    ],
    preprocessors: {
      "./spec/test/*.spec.jsx": ["webpack"]
    },
    //webpack: webpackConfig,
      webpack: {
          module: {
              loaders: [
                  { test: /\.jsx$/, loader: 'babel-loader' }
              ]
          },
          entry: {},
          plugins: [
              new webpack.ResolverPlugin([
                  new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("package.json", ["main"])
              ])
          ],
          resolve: {
              root: [path.join(__dirname, "./node_modules"), path.join(__dirname, "./app")]
          }
      },
    webpackMiddleware: {
      noInfo: true
    },
    plugins: [
      require("karma-webpack"),
      require("karma-jasmine"),
      require("karma-chrome-launcher")
    ],
    reporters: ["dots"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: false
  });
};