var webpack = require('webpack');

//create variable that holds the directory to bower_components
var bower_dir = __dirname + '/bower_components';

var config = {
    addVendor: function (name, path) {
        this.resolve.alias[name] = path;
        this.module.noParse.push(new RegExp('^' + name + '$'));
    },
    entry: ['./app/main'],
    vendors: ['react', 'jquery'],
    resolve: {
        alias: {},
        extensions: ['', '.js', '.jsx']

    },
    // We add a plugin called CommonsChunkPlugin that will take the vendors chunk
    // and create a vendors.js file. As you can see the first argument matches the key
    // of the entry, "vendors"
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
    ],
    output: {
        path: './build',
        filename: 'bundle.js'
    },
    module: {
        noParse: [],
        loaders: [
            { test: /\.jsx$/, loader: 'babel-loader' }
        ]
    }
};
config.addVendor('jquery', bower_dir + '/jquery/dist/jquery.min.js');
config.addVendor('react', bower_dir + '/react/react.min.js');



module.exports = config;