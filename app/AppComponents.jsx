var React = require('react');

var Component = React.createClass({
    render: function () {
        return (
            <h1>Hello world!</h1>
        );
    }
});

module.exports = Component;